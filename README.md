# IAC 3 - RDS



## Objetivo:
- Criar um modulo terraform que chame outros 3 modulos:
    - Criaçao de EC2
    - Criaçao de um RDS
    - Criaçao de uma fila SQS

- Cada modulo deve receber pelo menos 3 variaveis e fazer pelo menos 1 output
- Todos os modulos precisam estar dispoveis no Git - modulo remoto
- Chamada para os modulos precisam ser via GIT

## Pré Reqs:
- Instalar o Terraform (https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli) na maquina onde será orquestrado o provisionamento da infra
- Ter um usuario programatico na AWS (https://docs.aws.amazon.com/pt_br/IAM/latest/UserGuide/id_users_create.html)

## Execucão:
- Baixar os arquivos deste repositório:
```
git clone https://gitlab.com/feliperw/iac-3-rds.git
```

- Exporte as variáveis de ambiente para acesso a AWS:
```
export AWS_ACCESS_KEY_ID="{Insira o ID do usuário programático da AWS criado nos passos anteriores}"
export AWS_SECRET_ACCESS_KEY="{Insira a Secret do usuário programático da AWS criado nos passos anteriores}"
```

- Exporte as variáveis de ambiente referente a configuração da VM:
```
export TF_VAR_subnet_ids="{[IDs das subnets]}"
export TF_VAR_allocated_storage="{Tamanho do Storage a ser alocado}"
export TF_VAR_db_name="{Nome da Instancia}"
export TF_VAR_sg_id_rds="{ID do Grupo de Segurança}"
export TF_VAR_engine="{Tipo do banco a ser utilizado}"
export TF_VAR_instance_class="{Shape da Instancia}"
export TF_VAR_username="{Usuario}"
export TF_VAR_password="{Senha}"
```
- Execute os comandos do Terraform para criar a infra:
```
terraform init
terraform plan
terraform apply
```

- Após a criação da infra, será exibido o IP de cada VM, endereço de DNS do LB e endpoint do RDS

```
rds_endpoint = ""
```

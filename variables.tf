variable "subnet_ids" {
  default     = [""]
  type        = list(string)
  description = "Subnets IDs for RDS"
}

variable "allocated_storage" {
  default     = ""
  type        = string
  description = "Storage size for RDS"
}

variable "db_name" {
  default     = ""
  type        = string
  description = "Name for RDS Instance"
}

variable "sg_id" {
  default     = ""
  type        = string
  description = "Security Group ID for RDS"
}

variable "engine" {
  default     = "postgres"
  type        = string
  description = "Engine for RDS"
}

variable "instance_class" {
  default     = "db.t3.micro"
  type        = string
  description = "Class for RDS"
}

variable "username" {
  default     = ""
  type        = string
  description = "Username for RDS"
}

variable "password" {
  default     = ""
  type        = string
  description = "Password for RDS"
}
resource "aws_db_subnet_group" "rdssn_iac_01" {
  name       = "rdssn-iac-01"
  subnet_ids = var.subnet_ids

  tags = {
    Name = "rdssn-iac-01"
  }
}

resource "aws_db_instance" "rds_iac_01" {
  allocated_storage      = var.allocated_storage
  db_name                = var.db_name
  db_subnet_group_name   = aws_db_subnet_group.rdssn_iac_01.name
  vpc_security_group_ids = [var.sg_id]
  engine                 = var.engine
  instance_class         = var.instance_class
  username               = var.username
  password               = var.password
  skip_final_snapshot    = true
}
